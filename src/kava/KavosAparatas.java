package kava;

import gyvunas.JuodasKatinas;
import puodelisPackage.*;

public class KavosAparatas {
    public Produktai produktai;
//    Sukurti kavos aparatą:​
//    Kuris turėtų atributus:​
//    Cukraus kiekis​
//    Kavos pupelių kiekis​
//    Vandens kiekis​
//    Panaudojimų skaičius​
//    Panaudojimų skaičiaus riba – konstanta, kuri saugo, kas kiek panaudojimų reikia atlikti plovimą​
/*    public int cukrausKiekisG;
    public int kavosPupeliuKiekisG;
    public int vandensKiekisL;
    public int panaudojimuSkaicius;
    public static final int PANAUDIJOMORIBA=20;*/

    public KavosAparatas() {
        this.produktai = new Produktai();
    }

    public KavosAparatas(Produktai produktai) {
        this.produktai = produktai;
    }

    public void priskirtiProduktuObjekta(Produktai produktaiBendri){
        this.produktai=produktaiBendri;
    }


    //        Kavos aparato gaminkKava metodas turėtų:​
//        Sukurti atitinkamo tipo kavos puodelį​
//        Sukurti metodą gamink kavą, kuris priima kavos puodelį, kuris pagal kavos puodelio produkto kiekius sumažintų kavos aparato produktų kiekius ir pakeistų ‚ar kava pagaminta‘ į true​
//        Grąžintų atitinkamo tipo kavos puodelį su pagaminta kava
    public boolean gaminkKava(KavosPuodelis kazkokiosKavosPuodelis){
        Produktai prod=kazkokiosKavosPuodelis.gautiKavosPuodelioProduktai();
        return gamintiKava(prod,kazkokiosKavosPuodelis.kavosPavadinimas);
    }

/*    public int bubu(){
        return 0;
    }
    public String bubu(int i){
        return "0";
    }*/

/*    public kava.KavosAparatas(int cukrausKiekisG,int kavosPupeliuKiekisG,int vandensKiekisL,int panaudojimuSkaicius){
        this.cukrausKiekisG=cukrausKiekisG;
        this.kavosPupeliuKiekisG=kavosPupeliuKiekisG;
        this.vandensKiekisL=vandensKiekisL;
        this.panaudojimuSkaicius=panaudojimuSkaicius;
    }
    public kava.KavosAparatas(){

    }*/

//    Kuris mokėtų:​
//    Papildyti vandens/pupelių/cukraus​
    public void papildyti(int vandensKiekisL,int kavosPupeliuKiekisG,int cukrausKiekisG){
        if (vandensKiekisL+this.produktai.vandensKiekisL>20) {
            System.out.println("Per daug vandens. Netelpa. Dabar yra "+this.produktai.vandensKiekisL+ ".Galima papildyti "+(20-this.produktai.vandensKiekisL));
        }else{
            setVandensKiekisL(this.produktai.vandensKiekisL+vandensKiekisL);
            System.out.println("Papildyta vandens "+vandensKiekisL+ ". Dabar yra "+this.produktai.vandensKiekisL);
        }
        if (kavosPupeliuKiekisG+this.produktai.kavosPupeliuKiekisG>20) {
            System.out.println("Per daug kavos pupeliu. Netelpa. Dabar yra "+this.produktai.kavosPupeliuKiekisG+ ".Galima papildyti "+(20-this.produktai.kavosPupeliuKiekisG));
        }else{
            this.produktai.kavosPupeliuKiekisG+=kavosPupeliuKiekisG;
            System.out.println("Papildyta kavos pupeliu "+kavosPupeliuKiekisG+ ". Dabar yra "+this.produktai.kavosPupeliuKiekisG);
        }
        if (cukrausKiekisG+this.produktai.cukrausKiekisG>20) {
            System.out.println("Per daug cukraus. Netelpa. Dabar yra "+this.produktai.cukrausKiekisG+ ".Galima papildyti "+(20-this.produktai.cukrausKiekisG));
        }else{
            this.produktai.cukrausKiekisG+=cukrausKiekisG;
            System.out.println("Papildyta cukraus "+cukrausKiekisG+ ". Dabar yra "+this.produktai.cukrausKiekisG);
        }

    }
/*    public void papildyti(){
        this.vandensKiekisL=vandensKiekisL;
        this.kavosPupeliuKiekisG=kavosPupeliuKiekisG;
        this.cukrausKiekisG=cukrausKiekisG;
    }*/
//    Gaminti bent trijų rūšių kavą (nustatoma pagal String parametrą ir kuri naudotų skirtingus produktų kiekius), jeigu trūksta nors vieno produkto 0 atspausdintų atitinkamą pranešimą;
//    Taip pat atspausditų pranešimą jeigu laikas plovimui​
    public boolean gamintiKava(Produktai produktai, String kavosPavadinimas){
        if (this.produktai.vandensKiekisL<produktai.vandensKiekisL) {
            System.out.println("Per mazai vandens. Papildykite: " +(produktai.vandensKiekisL-this.produktai.vandensKiekisL));
            return false;
        } else if (this.produktai.kavosPupeliuKiekisG<produktai.kavosPupeliuKiekisG) {
            System.out.println("Per mazai kavos pupeliu. Papildykite: " + (produktai.kavosPupeliuKiekisG - this.produktai.kavosPupeliuKiekisG));
            return false;
        } else if (this.produktai.cukrausKiekisG<produktai.cukrausKiekisG) {
            System.out.println("Per mazai cukraus. Papildykite: " + (produktai.cukrausKiekisG - this.produktai.cukrausKiekisG));
            return false;
        }else if (this.produktai.panaudojimuSkaicius>this.produktai.PANAUDIJOMORIBA) {
            System.out.println("Aparatą reikia plauti");
            return false;
        } else {
            System.out.println(kavosPavadinimas+" paruosta.");
            this.produktai.panaudojimuSkaicius++;
            this.produktai.vandensKiekisL--;
            this.produktai.cukrausKiekisG--;
            this.produktai.kavosPupeliuKiekisG--;
            return true;
        }
    }
//    Atlikti plovimą (apnulina panaudojimų skaičių)​
    public void plovimas(){
        this.produktai.panaudojimuSkaicius=0;
        this.produktai.kavosPupeliuKiekisG=0;
        this.produktai.cukrausKiekisG=0;
        this.produktai.vandensKiekisL=0;
    }
//    Pasakyti ar pasiruošęs (netrūksta produktų ir išsivalęs)​
    public void arPasiruoses(){
        if (this.produktai.panaudojimuSkaicius==0&&this.produktai.cukrausKiekisG>5&&this.produktai.kavosPupeliuKiekisG>5&&this.produktai.vandensKiekisL>5){
            System.out.println("Kavos aparatas pasiruoses, nieko netruksta.");
        }else{
            System.out.println("Kavos aparatas nepasiruoses.");
        }
    }
//    Pasakytų kokia dabar produktų būsena – atspausdintų informaciją​
    public void produktuBusena(){
        System.out.println("Vandens"+this.produktai.vandensKiekisL+", Kavos:"+this.produktai.kavosPupeliuKiekisG+", Cukraus:"+this.produktai.cukrausKiekisG);
    }
//    Pasakytų kokia yra aparato būsena (kiek liko produktų ir iki plovimo)​
    public void aparatoBusenaimas(){
        System.out.println("Liko panaudoti kartu iki plovimo:"+(this.produktai.PANAUDIJOMORIBA-this.produktai.panaudojimuSkaicius));
    }
//    Turėtų keletą konstruktorių​
//    Turėtų set ir get metodus laukams​


    public void setCukrausKiekisG(int cukrausKiekisG) {
        this.produktai.cukrausKiekisG = cukrausKiekisG;
    }

    public void setKavosPupeliuKiekisG(int kavosPupeliuKiekisG) {
        this.produktai.kavosPupeliuKiekisG = kavosPupeliuKiekisG;
    }

    public void setVandensKiekisL(int vandensKiekisL) {
        this.produktai.vandensKiekisL = vandensKiekisL;
    }

    public void setPanaudojimuSkaicius(int panaudojimuSkaicius) {
        this.produktai.panaudojimuSkaicius = panaudojimuSkaicius;
    }

    public int getCukrausKiekisG() {
        return produktai.cukrausKiekisG;
    }

    public int getKavosPupeliuKiekisG() {
        return produktai.kavosPupeliuKiekisG;
    }

    public int getPanaudojimuSkaicius() {
        return produktai.panaudojimuSkaicius;
    }

    public int getVandensKiekisL() {
        return produktai.vandensKiekisL;
    }

}
