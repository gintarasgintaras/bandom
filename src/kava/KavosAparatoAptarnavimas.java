package kava;

public class KavosAparatoAptarnavimas {
//    Sukurti kavos aparatus aptarnaujančią klasę:​
//    Turėtų metodą sukurk aparatą ir grąžintų sukurtą aparatą.​
    public KavosAparatas createKavosAparatas(){
        return new KavosAparatas();
    }

//    Turėtų medotą išplauk kavos aparatą kuris priimtų kavos aparatą.​
    public void isplaukKavosAparata(KavosAparatas kavosAparatas){
        kavosAparatas.produktai.panaudojimuSkaicius=0;
        kavosAparatas.produktai.cukrausKiekisG=0;
        kavosAparatas.produktai.vandensKiekisL=0;
        kavosAparatas.produktai.kavosPupeliuKiekisG=0;
        //arba  apnulintiAparata() ;
    }
//    Leistų sukurti n aparatų, ir sukurtus aparatus gražintų kaip masyvą.​
    public KavosAparatas[] arrayAparatai(int kiekis){
        KavosAparatas[] kavosAparatas = new KavosAparatas[kiekis];
        for (int i=0;i<kiekis;i++){
            kavosAparatas[i]=createKavosAparatas();
        }
        return kavosAparatas;
    }
//    Kuri išvalytų visus paduotų aparatų produktus ir pasakytų sumą kiek ko išvalė.​
    public void isvalykProduktusMasyvo(KavosAparatas[] kavosAparatuMasyvas){
        for (int i=0;i<kavosAparatuMasyvas.length;i++){
            System.out.println(" ");
            System.out.print("Pries plovima buvo:");
            System.out.print(" kavos pupeliu "+kavosAparatuMasyvas[i].produktai.kavosPupeliuKiekisG);
            System.out.print(", vandens "+kavosAparatuMasyvas[i].produktai.vandensKiekisL);
            System.out.print(", cukraus "+kavosAparatuMasyvas[i].produktai.cukrausKiekisG);
            System.out.println(" ");
            kavosAparatuMasyvas[i].plovimas();
        }
    }

//    Išplautų visus per masyvą perduotus aparatus.​

      //panasus kaip ir pries tai

//    Visiems aparatams priskirtų tą patį produktų objektą.​
    public void priskirkProduktusVisiemsVienodus(KavosAparatas[] kavosAparatuMasyvas, Produktai produktai){
        for (int i=0;i<kavosAparatuMasyvas.length;i++){
            kavosAparatuMasyvas[i].priskirtiProduktuObjekta(produktai);
        }
    }
//    Visiems aparatams priskirtų skirtingus produktų objektus.​
    public void priskirkProduktusVisiemsSkirtingus(KavosAparatas[] kavosAparatuMasyvas, Produktai[] produktai){
        for (int i=0;i<kavosAparatuMasyvas.length;i++){
            kavosAparatuMasyvas[i].priskirtiProduktuObjekta(produktai[i]);
        }
    }
//    Klases išskaidyti į skirtingus paketus ir klasėmis manipuoliuoti atskiroje Main klasėje.​

//    Atkreipti dėmesį į klasių, kintamųjų, metodų ir paketų vardus.


}
