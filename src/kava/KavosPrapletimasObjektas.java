package kava;

public class KavosPrapletimasObjektas {
    public static void main(String[] args) {
        //KavosPrapletimasObjektas kavosPrapletimasObjektas=new KavosPrapletimasObjektas();
        KavosAparatoAptarnavimas kavosAparatoAptarnavimas=new KavosAparatoAptarnavimas();
        KavosAparatas naujasKavosAparatas=kavosAparatoAptarnavimas.createKavosAparatas();

        naujasKavosAparatas.papildyti(10,10,10);

        naujasKavosAparatas.arPasiruoses();

        System.out.println(" ");
        KavosAparatas[] aparatasx = kavosAparatoAptarnavimas.arrayAparatai(2);
        KavosAparatas pirmas=aparatasx[0];
        KavosAparatas antras=aparatasx[1];
        pirmas.papildyti(11,11,11);
        antras.papildyti(13,13,13);

        System.out.println(" ");
        pirmas.produktuBusena();
        antras.produktuBusena();
        kavosAparatoAptarnavimas.isvalykProduktusMasyvo(aparatasx);
        System.out.println(" ");
        pirmas.produktuBusena();
        antras.produktuBusena();

        Produktai produktaiNaujasVisiems=new Produktai();
        produktaiNaujasVisiems.vandensKiekisL=15;
        produktaiNaujasVisiems.cukrausKiekisG=16;
        produktaiNaujasVisiems.kavosPupeliuKiekisG=17;
        kavosAparatoAptarnavimas.priskirkProduktusVisiemsVienodus(aparatasx, produktaiNaujasVisiems);
        System.out.println(" ");
        System.out.println("Produktai priskirti visiems aparatams vienodi:");
        pirmas.produktuBusena();
        antras.produktuBusena();


        Produktai[] produktaiSkirtingi=new Produktai[2];
            for(int i=0;i<2;i++){
                //sugeneruoju skirtingus skaicius skirtingiems aparatams
                produktaiSkirtingi[i]=new Produktai();
                produktaiSkirtingi[i].kavosPupeliuKiekisG=2*(i+1);
                produktaiSkirtingi[i].vandensKiekisL=3*(i+1);
                produktaiSkirtingi[i].cukrausKiekisG=4*(i+1);
            }
        kavosAparatoAptarnavimas.priskirkProduktusVisiemsSkirtingus(aparatasx,produktaiSkirtingi);
        System.out.println(" ");
        System.out.println("Produktai priskirti visiems aparatams skirtingi:");
        pirmas.produktuBusena();
        antras.produktuBusena();

    }
}
