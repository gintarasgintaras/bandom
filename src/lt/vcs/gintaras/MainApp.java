package lt.vcs.gintaras;

import java.util.Scanner;

public class MainApp {
    public static void main(String[] args) {

        Minutes2.minuciuKonvertavimas(1000);
//        Atskirame lt.vcs.vardas. pakete  sukurti klasę Skaičiavimai kuri turėtų žemiau nurodytus metodus su static raktažodžiais. Metodus kviesti iš kitos klasės main metodo.

//        Pasakytų ar perduotas skaičius lyginis ar ne.​
        System.out.println("Ar lyginis?"+Skaiciavimas.arLyginis(71));
        System.out.println(" ");
//        Rastų perduoto skaičiaus paskutinį skaitmenį.​
        System.out.println("Paskutinis skaitmuo:"+Skaiciavimas.raskPaskutini(12546));
        System.out.println(" ");
//        Rastų perduoto skaičiaus pirmą skaitmenį.​
        System.out.println("Pirmas skaitmuo:"+Skaiciavimas.raskPirma(45126));
        System.out.println(" ");
//        Pasakytų keliaženklis skaičius paduotas.​
        System.out.println("Kiek zenklu turi skaitmuo:"+Skaiciavimas.keliazenklis(45126));
        System.out.println(" ");
//        Metodą kuris priimtų du parametrus – daliklį ir kiek skaičių atspausdinti, bei atspausdintų tiek iš eilės einančių skaičių, kurie dalijasi iš daliklio.
//        Metodas turėtų didinti skaitliuką ir atspausdinti skaitmenis kurie dalinasi iš daliklio.​
        Skaiciavimas.daliklisKiekSkaiciu(4,5);
        System.out.println(" ");
//        Atspausdintų int skaičiaus skaičius atvirkščia tvarka.​
        Skaiciavimas.atvirkstineTvarka(65425);
        System.out.println(" ");
//        Sukurti atskirą minučių konvertavimo klasę, kuri atspausdintų kiek valandų ir dienų yra pagal paduotą minučių skaičių.​
        //Skaiciavimas.kiekDienu(1530);
        Minutes.kiekDienu(1530);
        System.out.println(" ");
//        Sukurti metodą, kuris prima sveiką skaičių – minutes ir atspausdintų kiek dienų, valandų ir minučių praeis. (Pvz.: įvedus 123 -> turėtų atspausdinti: 0 dienų, 2 valandos, 3 minutės).​
        System.out.println(" ");
        Skaiciavimas.kiekPraeis(1530);
        System.out.println(" ");
//        Sukurti metodą kuris paprašytų įvesti dienas, valandas ir minutes ir paskaičiuotų kiek tai yra minučių.
        System.out.println("Suveskite dienas, valandas,minutes:");
        Scanner scaner = new Scanner(System.in);
        int dienos=Integer.valueOf(scaner.nextLine());
        int valandos=Integer.valueOf(scaner.nextLine());
        int minutes=Integer.valueOf(scaner.nextLine());
        Skaiciavimas.kiekMinuciu2(dienos,valandos,minutes);
    }
}
