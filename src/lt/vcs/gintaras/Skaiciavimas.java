package lt.vcs.gintaras;

import java.util.Scanner;

public class Skaiciavimas {

//        Atskirame lt.vcs.vardas. pakete  sukurti klasę Skaičiavimai kuri turėtų žemiau nurodytus metodus su static raktažodžiais. Metodus kviesti iš kitos klasės main metodo.
/////////////////////////////////////////////////////////////

//        Pasakytų ar perduotas skaičius lyginis ar ne.​
          static String arLyginis(int skaicius) {
              String lyginis = "Ne";
              if (skaicius%2 == 0) lyginis = "Taip";
              return lyginis;
          }

//        Rastų perduoto skaičiaus paskutinį skaitmenį.​
          static String raskPaskutini(Integer skaicius) {
               String skaiciusString=String.valueOf(skaicius);
               String atsakymas=skaiciusString.substring(skaiciusString.length()-1, skaiciusString.length());
              return atsakymas;

          }

//        Rastų perduoto skaičiaus pirmą skaitmenį.​
        static String raskPirma(Integer skaicius) {
            String skaiciusString=String.valueOf(skaicius)      ;
            return skaiciusString.substring(0, 1);
        }
//        Pasakytų keliaženklis skaičius paduotas.​
        static int keliazenklis(Integer skaicius) {
            String skaiciusString=String.valueOf(skaicius)      ;
            return skaiciusString.length();
        }
//        Metodą kuris priimtų du parametrus – daliklį ir kiek skaičių atspausdinti, bei atspausdintų tiek iš eilės einančių skaičių, kurie dalijasi iš daliklio.
//        Metodas turėtų didinti skaitliuką ir atspausdinti skaitmenis kurie dalinasi iš daliklio.​
        static void daliklisKiekSkaiciu(int daugiklis,int kiekSKaiciu) {
            for (int i=1;i<=kiekSKaiciu;i++){
                System.out.print(i*daugiklis+" ");
            }

        }

//        Atspausdintų int skaičiaus skaičius atvirkščia tvarka.​
        static void atvirkstineTvarka(int skaicius) {
            String skaiciusString=String.valueOf(skaicius)      ;
            for(int i=skaiciusString.length()-1;i>=0;i--){
                System.out.print(skaiciusString.substring(i, i+1)+" ");
            };

/*            int inv=0
            while(skaicius>0){
                inv=10*inv+skaicius%10;
                skaicius=skaicius/10;
            }*/
        }

//        Sukurti atskirą minučių konvertavimo klasę, kuri atspausdintų kiek valandų ir dienų yra pagal paduotą minučių skaičių.​
        static void kiekDienu(int skaiciusMinutes) {
            Integer valandu=skaiciusMinutes/60;
            Integer dienu=valandu/24;
            System.out.println("Valandu:"+valandu);
            System.out.println("Dienu:"+dienu);
            }

//        Sukurti metodą, kuris prima sveiką skaičių – minutes ir atspausdintų kiek dienų, valandų ir minučių praeis. (Pvz.: įvedus 123 -> turėtų atspausdinti: 0 dienų, 2 valandos, 3 minutės).​

    static void kiekPraeis(int skaiciusMinutes) {
        Integer dienu=skaiciusMinutes/60/24;
        Integer valandu=skaiciusMinutes/60-dienu*24;
        Integer minutes=skaiciusMinutes-dienu*24*60-valandu*60;
        System.out.println("Dienu:"+dienu);
        System.out.println("Valandu:"+valandu);
        System.out.println("Minuciu:"+minutes);
    }
//        Sukurti metodą kuris paprašytų įvesti dienas, valandas ir minutes ir paskaičiuotų kiek tai yra minučių.
    static void kiekMinuciu2(int skaiciusDienos,int skaiciusValandos,int skaiciusMinutes) {
        Integer minutes=skaiciusDienos*24*60+skaiciusValandos*60+skaiciusMinutes;
        System.out.println("Minuciu2:"+minutes);
    }

}


