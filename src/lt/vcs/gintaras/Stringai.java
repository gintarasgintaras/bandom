package lt.vcs.gintaras;

import java.util.Scanner;

public class Stringai {
    public static void main(String[] args) {
        Scanner scaner = new Scanner(System.in);
        System.out.println("Iveskite teksta:");
        String reiksme=scaner.nextLine();
//        Sukurti programą, kuri nuskaito tekstą iš konsolės ir tada atlieka žemiau išvardintus veiksmus ir atspausdina  pranešimus/informaciją visais atvejais:​

//        Atspausdinti String teksto ilgį​
        System.out.println("Ilgis:"+reiksme.length());
//        Atspausdinti a raidės indeksą​
        System.out.println("a raides indeksas:"+reiksme.indexOf('a'));
//        Atspausdinti paskutinės nuo galo a raidės indeksą​
        System.out.println("a raides indeksas paskutinis nuo galo:"+reiksme.lastIndexOf('a'));
//        Nuvalyti String pradžioje/gale esančius tarpus „   TEST “ -> „TEST“​
        System.out.println("Nuvalyti String pradžioje/gale esančius tarpus:"+reiksme.trim());
//        Paversti tekstą į didžiasias/mažasias raides​
        System.out.println("Paversti tekstą į didžiasias raides:"+reiksme.toUpperCase());
        System.out.println("Paversti tekstą į mažasias raides:"+reiksme.toLowerCase());
//        Patikrinti ar tekstas prasideda/baigiasi abc​
        if (reiksme.indexOf("abc")==0){
            System.out.println("Patikrinti ar tekstas prasideda/abc: Taip");
        }else{
            System.out.println("Patikrinti ar tekstas prasideda/abc: Ne");
        }


        if (reiksme.lastIndexOf("abc")==reiksme.length()){
            System.out.println("Patikrinti ar tekstas baigiasi/abc: Taip");
        }else{
            System.out.println("Patikrinti ar tekstas baigiasi/abc: Ne");
        }

        String reiksme2= reiksme.substring(0,2);
        String ats="Ne";
        if (reiksme2=="abc") {
             ats="Taip";
        }
        System.out.println("Patikrinti ar tekstas prasideda/abc:"+ats);
//        Patikrinti ar tekstas talpina „test“ tekstą viduje​
        String reiksme2_1= reiksme.substring(reiksme.length()-3,reiksme.length());
        String ats2_1="Ne";
        if (reiksme2_1=="abc") {
            ats2_1="Taip";
        }
        System.out.println("Patikrinti ar tekstas baigiasi abc:"+ats);
//        Tekste pakeisti visus abc į def​
        System.out.println("Tekste pakeisti visus abc į def:"+reiksme.replaceAll("abc","def"));
//        Surasti abc tekso indeksą/pradžios vietą​
        System.out.println("Surasti abc tekso indeksą/pradžios vietą:"+reiksme.indexOf("abc"));
//        Panaudoti substring medotą ir iškirpti tekstą iki abc, palikti tik antrą dalį​
        //System.out.println("Panaudoti substring medotą ir iškirpti tekstą iki abc, palikti tik antrą dalį:"+reiksme.substring(reiksme.indexOf("abc"),reiksme.length()));
//        Su substring iškirpti /atspausdintipirmą pusę teksto​
        //System.out.println("Su substring iškirpti /atspausdintipirmą pusę teksto:"+reiksme.substring(0,reiksme.length()/2));
//        Su substring iškirpti /atspausdinti antrą pusę teksto​
        //System.out.println("Su substring iškirpti /atspausdinti antrą pusę teksto:"+reiksme.substring(reiksme.length()/2,reiksme.length()));
//        Išskaidyti sakinį per tarpus ir atspausdinti​
        String stringas=reiksme.trim();
        while(stringas.indexOf(" ")>=0){
            System.out.println("Zodis:"+stringas.substring(0,stringas.indexOf(' ')));
            stringas=stringas.substring(stringas.indexOf(' ')+1,stringas.length());
        }
        System.out.println("Zodis:"+stringas);
        String[] textArray=reiksme.split(" ");
        for(String a:textArray){
            System.out.println("bum: "+a);
        }
        //reiksme.charAt(i)

    }
}
