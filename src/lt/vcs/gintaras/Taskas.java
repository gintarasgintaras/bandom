package lt.vcs.gintaras;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

public class Taskas {
    public int x;
    public int y;
    public String spalva;

//    Sukurti taško klasę:​
//    Atributai x, y koordinatėms saugoti​
//    String tipo kintamasis skirtas spalvai saugoti​
//    Metodas apskaičiuoti atstumui iki kito taško​
//    Metodas, kuris pasakytų kuriame ketvirtyje (Cartesian Coordinates) yra taškas​
//    Metodas, kuris pasakytų ar paduotas taškas priklauso tam pačiam plokštumos ketvirčiui ()​
//    Perrašyti equals() metodą​
//    Sukurti konstruktorius:​
//    Be parametrų​
//    Su x, y​
//    Su x, y ir spalva​
//    Su spalva

    public Taskas(){
        this.x=10;
        this.y=10;
        this.spalva="red";
    }
    public Taskas(int x, int y){
        this.x=x;
        this.y=y;
        this.spalva="red";
    }
    public Taskas(int x, int y, String spalva){
        this.x=x;
        this.y=y;
        this.spalva=spalva;
    }

    public Taskas(String spalva){
        this.x=10;
        this.y=10;
        this.spalva=spalva;
    }

    //    Metodas apskaičiuoti atstumui iki kito taško​
    public double apskaiciuokAtstumaIkiKitoTasko(int koordX, int koordY){
        double atstumas=0;
        int xx=abs(koordX-this.x);
        int yy=abs(koordY-this.y);
        atstumas=sqrt(xx*xx+yy*yy);
        return  atstumas;
        }
    public int kurisKetvirtis(int koordX, int koordY){
        int ats=0;
        if (koordX>0){
            if (koordY>0){
                ats=1;
            }else{
                ats=4;
            }
        }else{
            if (koordY>0){
                ats=2;
            }else{
                ats=3;
            }
        }
        return  ats;
    }
    //Metodas, kuris pasakytų ar paduotas taškas priklauso tam pačiam plokštumos ketvirčiui ()​
    public boolean arTasPatsKetvirtis(int koordX, int koordY){
        int ats=0;
        int ats2=0;
        if (koordX>0){
            if (koordY>0){
                ats=1;
            }else{
                ats=4;
            }
        }else{
            if (koordY>0){
                ats=2;
            }else{
                ats=3;
            }
        }

        if (this.x>0){
            if (this.y>0){
                ats2=1;
            }else{
                ats2=4;
            }
        }else{
            if (this.y>0){
                ats2=2;
            }else{
                ats2=3;
            }
        }
        if (ats==ats2) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }



}
