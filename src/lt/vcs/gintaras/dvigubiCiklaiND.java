package lt.vcs.gintaras;

public class dvigubiCiklaiND {

    public static void main(String[] args) {
//    ------------------------------------------------------------------------------------------------
//    Sukurti metodą, kuris pagal paduotus 2 int parametrus atspausdintų atitinkamo dydžio stačiakampį:​
//            # # # # #​
//            # # # # #​
//            # # # # #​
        atspausdinkStaciakampi(4,15);
        System.out.println(" ");
//    Sukurti metodą, kuris pagal  2 int parametrus atspausdintų:​
//            # # # # # # #​
//             # # # # # # #​
//            # # # # # # #​
//             # # # # # # ​
        atspausdinkKitokiStaciakampi(3,7);
        System.out.println(" ");
//    Sukurti metodą, kuris atspausdintų dvimatį masyvą.​
        atspausdinkDvimati();
        System.out.println(" ");
//    Sukurti metodą, kuris užpildytų dvimatį masyva daugybos lentelės duomenimis ir atspausdintų daugybos iki 9 lentelę.
        atspausdinkDaugybosLentele();
        System.out.println(" ");
//--------------------------------------------------------------------------------------------------------------------
//    Sukurti metodą:​
//    Apskaičiuotų paduoto skaičiaus faktorialą.​
//    Išspausdintų Fibonacci sekos skaičius (fibonacci sequence).​
//    Atspausdintų k-ąjį Fibonacci sekos skaitmenį.​
    faktorialas(8);

    //eglute
        eglute(5);

    }

    //////////////////////////Metodai//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    ------------------------------------------------------------------------------------------------
//    Sukurti metodą, kuris pagal paduotus 2 int parametrus atspausdintų atitinkamo dydžio stačiakampį:​
//            # # # # #​
//            # # # # #​
//            # # # # #​
    private static void atspausdinkStaciakampi(int skaicius1,int skaicius2){
        for (int i=0;i<skaicius1;i++){
            for(int j=0;j<skaicius2;j++){
                System.out.print("# ");
            }
            System.out.println(" ");
        }
    }


    //            ​
//    Sukurti metodą, kuris pagal  2 int parametrus atspausdintų:​
//            # # # # # # #​
//             # # # # # # #​
//            # # # # # # #​
//             # # # # # # ​
    private static void atspausdinkKitokiStaciakampi(int skaicius1,int skaicius2){
        int pagalbinis;
        for (int i=0;i<skaicius1;i++){
            if(i%2!=0){
                System.out.print(" ");
            }
            for(int j=0;j<skaicius2;j++){
                System.out.print("# ");
            }
            System.out.println(" ");
        }
    }

    private static void eglute(int skaicius1){
        int pagalbinis=1;
        System.out.println("Eglute:");
        for (int i=0;i<skaicius1;i++){
            for (int h=skaicius1-pagalbinis/2;h>=1;h--){
                System.out.print(" ");
            }
                for (int j=0;j<pagalbinis;j++) {
                    System.out.print("#");
                }
                pagalbinis+=2;
            System.out.println(" ");
        }
    }

    //    Sukurti metodą, kuris atspausdintų dvimatį masyvą.​
    private static void atspausdinkDvimati(){
        int masyvas[][]={{1,2,3,4,5},{9,6,5,4,3},{5,6,1,4,9}};
        System.out.println("Dvimatis masyvas:");
        for (int i=0;i<masyvas.length;i++){
            for(int j=0;j<masyvas[0].length;j++){
                System.out.print(masyvas[i][j]+" ");
            }
            System.out.println(" ");
        }
    }
//    Sukurti metodą, kuris užpildytų dvimatį masyva daugybos lentelės duomenimis ir atspausdintų daugybos iki 9 lentelę.
    private static void atspausdinkDaugybosLentele(){
        int masyvas[][]= new int[9][9];
        System.out.println("Daugybos lentele:");

        System.out.println("   1  2  3  4  5  6  7  8  9");
        System.out.println("   --------------------------");
        String tarpas=" ";
        for (int i=0;i<masyvas.length;i++){
            System.out.print(i+1+"| ");
            for(int j=0;j<masyvas[0].length;j++){
                tarpas="";
                if ((i+1)*(j+1)<10) tarpas=" ";
                masyvas[i][j]=(i+1)*(j+1);
                System.out.print(((i+1)*(j+1))+tarpas+" ");
            }
            System.out.println(" ");
        }
    }

//--------------------------------------------------------------------------------------------------------------------
//    Sukurti metodą:​
//    Apskaičiuotų paduoto skaičiaus faktorialą.​
//    Išspausdintų Fibonacci sekos skaičius (fibonacci sequence).​
//    Atspausdintų k-ąjį Fibonacci sekos skaitmenį.​

    private static void faktorialas(int skaicius){
        int faktorialas=0;
        //for(int i=1;i<=skaicius;i++) faktorialas+=i;
        //System.out.println(skaicius+"!="+faktorialas);


        int fibernati=0;
        if (skaicius==1) fibernati=1;
        System.out.print("fibernati seka=0 1 ");
        int fib_2=0;
        int fib_1=1;
        for(int i=2;i<=skaicius;i++){
            fibernati=fib_1+fib_2;
            fib_2=fib_1;
            fib_1=fibernati;
            System.out.print(fibernati+" ");
        }

        System.out.println(" ");
        System.out.println("fibernati="+fibernati);

    }

}
