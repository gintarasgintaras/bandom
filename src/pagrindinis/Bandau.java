package pagrindinis;

import java.util.Scanner;

public class Bandau {
    public static void main(String[] args) {
        System.out.println(inInt("vienas du"));
    }
    private static Scanner newScan() {
        return new Scanner(System.in);
    }



    public static int inInt(String query) {
        int number = 0;
        boolean error = true;
        while (error) {
            System.out.println(query);
            String sNumber = newScan().nextLine();
            try {
                number = Integer.parseInt(sNumber.replace(" ", ""));
                error = false;
            } catch (Exception e) {
                if (sNumber.equals("")) {
                    System.out.println("\nJus nieko neivedete.");
                } else {
                    System.out.println("\nJus ivedete \"" + sNumber + "\" vietoj sveikojo skaiciaus.");
                }
            }
        }
        return number;
    }

}

