package pagrindinis;

import java.util.Scanner;

public class Ciklai {
    public static void main(String[] args) {
//        Penkis kartus atspausdinti „Hello World!“ panaudojant for ciklą.
        for(int i=1;i<=5;i++){
            System.out.println("Hello World!");
        }

        int kiek=1;
        while(kiek<=5){
            System.out.println("While-Hello World!");
            kiek++;
        }

        kiek=1;
        do {
            System.out.println("DoWhile-Hello World!");
            kiek++;
        }
        while(kiek<=5);

//        Nuskaityti įvestą skaičių ir tiek kartų atspausdinti atspausdinti „Hello World!“.
        System.out.println("Kiek kartu spausdinsite:");
        int skaicius=readNumber();

        for(int i=1;i<=skaicius;i++){
            System.out.println("Hello World!");
        }

        int j=1;
        while (j<=skaicius){
            System.out.println("For-Hello World!");
            j++;
        }

        int jj=1;
        do{
            System.out.println("DoWhile-Hello World!");
            jj++;
        } while (jj<=skaicius);

//        Atspausdinti skaičius 1-50 panaudojant for ciklą.
        System.out.println("Skaiciai 1-50:");
        for(int i=1;i<=50;i++){
            System.out.println(i);
        }
        int iw=1;
        while (iw<=50){
            System.out.println(iw);
            iw++;
        }

        int idw=1;
        do {
            System.out.println(idw);
            idw++;
        }
        while(idw<=50);

//        Atspausdinti skaičių sumą nuo 1-100, panaudoti for ciklą.
        System.out.println("Skaiciu suma 1-100:");
        int suma=0;
        for(int i=1;i<=100;i++){
            suma+=i;
        }
        System.out.println(suma);

/*        int sumad=0;
        int id2=1;
        while(id2<=100){
            sumad+=id2;
            id2++;
        }
        System.out.println("suma while="+sumad);

        int sumadw2=0;
        int idw2=1;
        do {
            sumadw2+=idw2;
            idw2++;
        }while(idw2<=100);
        System.out.println("suma do while="+sumadw2);*/

//        Atspausdinti skaičių sumą nuo 1-100 su while ciklu.
        System.out.println("Skaiciu suma 1-100 su while:");
        int suma2=0;
        int i=1;
        while(i<=100){
            suma2+=i;
            i++;
        }
        System.out.println(suma2);

//        Atspausdinti skaičių sumą nuo 1-100 su do while ciklu.
        System.out.println("Skaiciu suma do  while 1-100 :");
        int suma3=0;
        int ii=1;
        do {
            suma3+=ii;
            ii++;
        }while(ii<=100);

        System.out.println(suma3);

    }

    private static int readNumber(){ //throws IOException{
        Scanner scaner = new Scanner(System.in);
        String readNumber = scaner.nextLine();
        return Integer.valueOf(readNumber) ;
    }
}
