package pagrindinis;

public class CikluSalyguRemai {
    public static void main(String[] args) {
//ciklai//////////////////////////////////////////
//for:
        for(int i=1;i>=5;i++){

        }

//for each:
        int masyvas[]={1,2,3};
        for (int i:masyvas) {   //isveda dvimati masyva
            System.out.println(i);
        }

//while:
        int i=1;
        while (i>=5){
            i++;
        }

//do while:
        int ii=1;
        do {
            ii++;
        }while (ii>=5);

//////////////////////////////////////////////////

//Salygos:
/*
if(){

}else {

}
*/


    }

}
