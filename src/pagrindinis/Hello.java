package pagrindinis;

/*
public class Studentas{
    String vardas;
    String pavarde;
    int pazymis;

    public void eik(){
        System.out.println("studentas eina");
    }
    public void gautiPazymi(int pazymis){
        this.pazymis =pazymis;
    }

    public void parodykpazymiuknygele(){
        System.out.println(vardas+pavarde+pazymis);
    }

}
*/
public class Hello {
    public static void main(String[] args) {
        //Studentas studentasPetras =new Studentas();
        //studentasPetras.eik();
        //studentasPetras.gautiPazymi(5) ;
        //studentasPetras.parodykpazymiuknygele();

        //Integer masyvas[][]=new Integer[2][];
        //Integer masyvas[0]=new Integer [3];
        int masyvas[][]={{1,2,3},
                         {4,5,6,7,8},
                         {9,0}};

        //System.out.println("skaicius="+masyvas[0][0]);
        //System.out.println("skaicius="+masyvas[0][1]);
        //System.out.println("skaicius="+masyvas[2][2]);

        for (int[] i:masyvas){   //isveda dvimati masyvas
            for (int j:i){
                System.out.print(" "+j+" ");
            }
            System.out.println("");
            System.out.println("----------");
        }

        System.out.print("   0 0  = "+masyvas[0][0]);
        System.out.print("   0 1  = "+masyvas[0][1]);
        System.out.print("   0 2  = "+masyvas[0][2]);
        System.out.println("   1 0  = "+masyvas[1][0]);
        System.out.print("   1 1  = "+masyvas[1][1]);
        System.out.println("   1 2  = "+masyvas[1][2]);
        System.out.println("----------");

/////////////////////////////////////////////////////////
        System.out.println("Nuo 10");
        int i=0,j=0;
        int mas[][]=new int[10][8];
        while(i<10){
            while(j<8){
                mas[i][j]=10-2*(i+j);
                j++;
            }
            i++;j=0;
        }
/////////////////////////////////////////////////////////
        for (int[] iii:mas){   //isveda dvimati masyvas
            for (int jjj:iii){
                System.out.print(" "+jjj+" ");
            }
            System.out.println("");
            System.out.println("----------");
        }

        //int masyvas[1]=new int [2];

/*        masyvas[0][0]=1;
        masyvas[0][1]=2;
        masyvas[0][2]=3;
        masyvas[1][0]=4;
        masyvas[1][1]=5;*/


        //int masyvas[1][]=new int [4];
        //masyvas[0]=1;
        Student student =new Student();
        student.nustatytiPazymiuVidurki(3,2,3);
        student.nustatytiKoda("12345678901");
        student.ivestiIrSuskaiciuotiVidurki ();

/*1
        String name = "Gintaras";
        String lastname = "Čepas";
        int year = 1234;
        int month = 6;
        int days = 26;
        String personalCode = "12345678901l";
        long ilgas = 124775754;
                System.out.println("-------------------------------");
        System.out.println("Asmens kortelė");
        System.out.println("Vardas:" + name + " Pavardė:" + lastname);
        System.out.println("Gim.data:" + year + "." + month + "." + days);
        System.out.println("Asm.kodas: " + personalCode);
        System.out.println("-------------------------------");
//Integer.valueOf()
        int masyvas[]=new int [2];
        //int masyvas[][]=new int [3];
        masyvas[0]=1;
*/


/*
        System.out.println("Naujas vardas ir Pavarde:");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try{
            name = br.readLine();

        }
        catch(Exception e){
            System.out.println("Write an integer number"); //This is what user will see if he/she write other thing that is not an integer
        }

        System.out.println("-------------------------------");
        System.out.println("Asmens kortelė");
        System.out.println("Vardas Pavardė:" + name);
        System.out.println("Gim.data:" + year + "." + month + "." + days);
        System.out.println("Asm.kodas: " + personalCode);
        System.out.println("-------------------------------");
*/
    }

}
