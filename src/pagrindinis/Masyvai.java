package pagrindinis;

import java.util.Scanner;

public class Masyvai {
    public static void main(String[] args) {
//        Sukurti programą kuri išpausdintų visus masyvo inicializuoto kaip „int masyvas [] = {skaičiai}“ narius.​
        int masyvas[]={1,3,5,1};
        for (int i:masyvas) {
            System.out.println(i);
        }

//        Sukurti programą atspausdintų masyvo narių sumą.​
        int suma=0;
        for (int i:masyvas) {
            suma+=i;
        }
        System.out.println("suma="+suma);

//        Sukurti programą kuri perrašo kintamuosius iš vieno masyvo į kitą.​
        //int masyvas[]={1,3,5,1};
        int masyvas2[]=new int[4];
        //int masyvas2[];
        int nr=0;
        for (int i:masyvas) {
            masyvas2[nr]=i;
            nr++;
        }
        //masyvas2=masyvas; //Tiesiog priskiria
        System.out.println("masyvas2:");
        for (int i:masyvas2) {
            System.out.println(i);
        }


        /*

        */


//        Sukurti programą kur skaitytų skaičius iš komandinės eilutės ir išsaugotų juos į 5 ilgio masyvą ir tada atspausdintų masyvo narius.​
        Scanner scaner = new Scanner(System.in);
        System.out.println("suveskite 5 masyvo skaicius:");
        int masyvas5[]=new int[5];
        for (int i=0;i<masyvas5.length;i++){
            masyvas5[i]=Integer.valueOf(scaner.nextLine());
        }
        System.out.println("masyvas5:");
        for (int i:masyvas5) {
            System.out.println(i);
        }

//
//        Sukurti programą, kuri iš pradžių paprašytų įvesti kiek skaičių reikės įvesti (į kokio dydžio masyvą) ir tada iš komandinės eilutės nuskaitytų visus skaičius.
//        Pabaigus skaitymą, atspausdinti masyvo narius, visų narių sumą, vidurkį ir sandaugą.
        Scanner scaner1 = new Scanner(System.in);
        System.out.println("Kiek noretumete ivesti skaiciu i masyva:");
        int masyvoElementuSkaicius=Integer.valueOf(scaner1.nextLine());
        int masyvas10[]=new int[masyvoElementuSkaicius];

        System.out.println("Suveskite "+ masyvoElementuSkaicius +" skaicius i masyva:");
        for (int i=0;i<masyvoElementuSkaicius;i++){
            masyvas10[i]=Integer.valueOf(scaner1.nextLine());
        }

        int visuNariuSuma=0;
        int sandauga=1;
        System.out.println("masyvas5:");
        for (int i:masyvas10) {
            visuNariuSuma+=i;
            sandauga*=i;
            System.out.println(i);
        }
        System.out.println("visuNariuSuma:"+visuNariuSuma);
        System.out.println("vidurkis:"+((0.0+visuNariuSuma)/masyvoElementuSkaicius));
        System.out.println("sandauga:"+sandauga);

    }
}
