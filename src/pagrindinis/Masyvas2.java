package pagrindinis;

import java.util.Arrays;

public class Masyvas2 {
    public static void main(String[] args) {
//        Sukurti programą kuri perrašo kintamuosius iš vieno masyvo į kitą.​
        int masyvas[]={1,3,5,1};
        int[] masyvas2 = Arrays.copyOf(masyvas, masyvas.length);
        //int masyvas2[]=new int[4];
        //int masyvas2[];


/*        int nr=0;
        for (int i:masyvas;) {
            masyvas2[nr]=i;
            nr++;
        }*/

/*        for (int i=0;i<masyvas.length;i++){
            masyvas2[i]=masyvas[i];
        }*/
        System.out.println("masyvas:");
        for (int i:masyvas) {
            System.out.print(i+" ");
        }

        System.out.println(" ");
        System.out.println("masyvas2:");
        for (int i:masyvas2) {
            System.out.print(i+" ");
        }

        masyvas[1]=-1;
        masyvas2[1]=0;

        System.out.println("masyvas:");
        System.out.println(" ");
        for (int i:masyvas) {
            System.out.print(i+" ");
        }

        System.out.println(":");
        System.out.println("masyvas2:");
        for (int i:masyvas2) {
            System.out.print(i+" ");
        }
    }

}
