package pagrindinis;

import java.util.Scanner;

public class Masyvas3 {
    public static void main(String[] args) {
//        Sukurti programą kur skaitytų skaičius iš komandinės eilutės ir išsaugotų juos į 5 ilgio masyvą ir tada atspausdintų masyvo narius.​
        Scanner scaner = new Scanner(System.in);
        System.out.println("suveskite 5 masyvo skaicius:");
        int masyvas5[]=new int[5];
        for (int i=0;i<masyvas5.length;i++){
            masyvas5[i]=Integer.valueOf(scaner.nextLine());
        }
        System.out.println("masyvas5:");
        for (int i:masyvas5) {
            System.out.println(i);
        }
    }
}
