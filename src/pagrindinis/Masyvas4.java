package pagrindinis;

import java.util.Scanner;

public class Masyvas4 {
    public static void main(String[] args) {
        Scanner skaneris=new Scanner(System.in);
        System.out.println("suveskite 5 masyvo skaicius:");
        //        Sukurti programą kur skaitytų skaičius iš komandinės eilutės ir išsaugotų juos į 5 ilgio masyvą ir tada
        // atspausdintų masyvo narius.​
        int masyvas[]=new int[5];
        String stringKintamasis;
        int skaicius;
        for (int i=0;i<5;i++){
            stringKintamasis=skaneris.nextLine();
            skaicius=Integer.valueOf(stringKintamasis);
            masyvas[i]=skaicius;
        }

        System.out.println("Masyvas:");
        for (int i=0;i<5;i++){
            System.out.print(masyvas[i]+" ");
        }
    }
}
