package pagrindinis;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class MasyvasND {
    public static void main(String[] args) {
//        1. Sukurti metodą, kuris patikrintų ar du paduoti masyvai yra lygus – pagal ilgį ir pagal reikšmes.
/*        int[] masyvas1={7,3,5,2};
        int[] masyvas2={7,3,5,2};
        if (arMasyvaiLygus(masyvas1,masyvas2)){
            System.out.println("Lygus");
        }else{
            System.out.println("Nelygus");
        }*/

//        2. Sukurti programą, kuri skaitytų sveikus skaičius nuo 0 iki 10 iki tol kol bus įvestas -1 ir tada atspausdintų kiek kokių skaičių buvo įvesta.
        //ivestiSveikusSkaicius0_10();


//        3. Sukurti paieškos masyve metodą, kuris priimtų sveikų skaičių masyvą ir sveiką skaičių kaip parametrus.
//        Grąžintų indeksą masyve (ieškoto skaičiaus vietą masyve) jeigu skaičius rastas, o jeigu ne – tada -1. Panaudoti break išėjimui iš paieškos ciklo.
        int masymas9[]={5,2,6,3,1};
        System.out.println(" ");
        System.out.println("Paieska:"+ieskokMasyve(masymas9,2));


//        4. Sukurti metodą, kuris konvertuotų perduotą sveikų skaičių masyvą į String ir grąžintų String parametrą – visi masyvo nariai, atskirti kableliu.
        int masymas10[]={5,2,6,3,1};
        System.out.println(" ");
        System.out.println("Konvertuotas masyvas:"+konvertuoti(masymas10));

    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        1. Sukurti metodą, kuris patikrintų ar du paduoti masyvai yra lygus – pagal ilgį ir pagal reikšmes.
    private static boolean arMasyvaiLygus(int[] masyvas1,int[] masyvas2){
        if(!(masyvas1.length==masyvas2.length)){
            return false;
        }
        for (int i=0;i<masyvas1.length;i++){
            if (!(masyvas1[i]==masyvas2[i])){
                return false;
            }
        }
        return true;

    }

//        2. Sukurti programą, kuri skaitytų sveikus skaičius nuo 0 iki 10 iki tol kol bus įvestas -1 ir tada atspausdintų kiek kokių skaičių buvo įvesta.
    private static void ivestiSveikusSkaicius0_10(){
        int masyvas[]=new int[0];
        int masyvas2[]={0,0,0,0,0,0,0,0,0,0,0};
        int masyvasVisi[]=new int[0];
        Scanner skaneris = new Scanner(System.in);
        boolean vykdyti=true;
        int skaicius,i=0,j=0;
        System.out.println("Iveskite skaicius");
        while(vykdyti){
            skaicius=Integer.valueOf(skaneris.nextLine());
            if(skaicius>=0&&skaicius<=10){
                masyvas = Arrays.copyOf(masyvas, j+1);
                masyvas[j]=skaicius;
                masyvas2[skaicius]=masyvas2[skaicius]+1;
            }
            masyvasVisi = Arrays.copyOf(masyvasVisi, i+1);
            masyvasVisi[i]=skaicius;


            if(skaicius==-1) vykdyti=false;
            i++;
        }

        System.out.println("Masyvas-ivesta0_10:");
        for (int ii=0;ii<masyvas.length;ii++){
            System.out.print(masyvas[ii]+" ");
        }
        System.out.println(" ");
        System.out.println("KiekKokiu0_10:");
        for(int iii=0;iii<masyvas2.length;iii++){
            System.out.println("Skaicius '"+iii+"':"+masyvas2[iii]+" vnt ");
        }

        System.out.println(" ");
        System.out.println("Masyvas_Visi:");
        for(int iii=0;iii<masyvasVisi.length;iii++){
            System.out.print(masyvasVisi[iii]+" ");
        }

    }


//        3. Sukurti paieškos masyve metodą, kuris priimtų sveikų skaičių masyvą ir sveiką skaičių kaip parametrus.
//        Grąžintų indeksą masyve (ieškoto skaičiaus vietą masyve) jeigu skaičius rastas, o jeigu ne – tada -1.
// Panaudoti break išėjimui iš paieškos ciklo.
    private static int ieskokMasyve(int[] masyvas5, int parametras) {
        int atsakymas=-1;
        for (int i = 0; i < masyvas5.length; i++) {
            if ((masyvas5[i] == parametras)) {
                atsakymas=i;
                break;
            }
        }
        return atsakymas;

    }

//        4. Sukurti metodą, kuris konvertuotų perduotą sveikų skaičių masyvą į String ir grąžintų String parametrą – visi masyvo nariai, atskirti kableliu.
    private static String konvertuoti(int[] masyvas10) {
        String atsakymas="";
        String res = Arrays.toString(masyvas10);

        return res;
    }


}
