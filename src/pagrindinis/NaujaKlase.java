package pagrindinis;

public class NaujaKlase {
    public static void main(String[] args) {
/*        int kintamasis=7;
        System.out.println("kintamasis="+kintamasis+" kintamasis++="+(++kintamasis));
        System.out.println("SveikuSkaiciuDalyba15/5="+15/5);
        System.out.println("SveikuSkaiciuDaugyba15*5="+15*5);
        String tekstinis="joreiksmeyrareiksme";
        System.out.println("String kintamojo reiksme="+tekstinis);
        String tekstinisKitas="kitaRreiksme";
        System.out.println("String sudetis:"+tekstinis+"+"+tekstinisKitas+"="+tekstinis+tekstinisKitas);
        double realus1=3.3;
        System.out.println("realus1="+realus1);
        double realus2=7.3;
        double realus=realus1+realus2;
        System.out.println("realus1+realus2:"+realus1+"+"+realus2+"="+realus);
        System.out.println("priimameGrazinameSveika="+priimtiGrazintiSveika(7));
        int paimtiSveika=priimtiGrazintiSveikaAtspausdinti(8);
        System.out.println("priimtiDuSveikusGrazintiSuma="+priimtiDuSveikusGrazintiSuma(7,8));
        System.out.println("priimti2CharusGrazintiStringa="+priimti2CharusGrazintiStringa('a','b'));
        priespaskutine("Labas");
        System.out.println("loginis="+loginis());*/

//Sukurti boolean kintamajį, priskirti pradinę reikšmę. Sukurti sąkinį, kuris jeigu kintamojo reikšmė true – atspausdintų „Tiesa“, jeigu false – atspausdintų „Netiesa“.
        boolean arTiesa=false;
        if (arTiesa) {
            System.out.println("Tiesa");
        }else{
            System.out.println("NeTiesa");
        }

//Parašyti  sąlygos sakinį, kuris atspausdintų pranešimą jeigu kintamojo reikšmė didesnė už 100.
        int kintamasis=105;
        if (kintamasis>100) {
            System.out.println("Tiesa");
        }

//Parašyti  sąlygos sakinį, kuris atspausdintų pranešimą jeigu  kintamojo reikšmė didesnė už 100, jeigu ne – tada atspausdintų kitą pranešimą.
        int kintamasis1=105;
        if (kintamasis1>100) {
            System.out.println("Tiesa");
        }else {
            System.out.println("neTiesa");
        }
//Parašyti sąlygos sakinį, kuris patikrintų ir  atspausdintų pranešimą „Skaicius yra  0...100 tarpe“ jeigu kintamojo reikšmė yra atitinkamame rėžyje.
// Kitu atveju atspausdintų pranešimą „Skaiciaus apibreztame rezyje nera“
        int kintamasis2=99;
        if ((kintamasis2>0)&&(kintamasis2<100)) {
            System.out.println("Skaicius yra  0...100 tarpe");
        }else {
            System.out.println("Skaiciaus apibreztame rezyje nera");
        }

//Sukurti boolean grąžinantį metodą pavadinimu checkIfNumberIsNegative. Metodas turi patikrinti ar skaičius neigiamas ir grąžinti rezultatą.
// Pagal gautą rezultatą atspausdinti pranešimą.
        boolean ar=checkIfNumberIsNegative(0);
        if (ar==true) {
            System.out.println("Neigiamas");
        }else{
            System.out.println("Teigiamas arba nulis");
        }

//Sukurti metodą, kuris patikrina ar paduotas skaičius yra triženklis.
        if (arTrizenklis(-123)) {
            System.out.println("Trizenklis");
        }else{
            System.out.println("NE trizenklis");
        }


//Sukurti metodą, kuris priima  tris int parametrus (sk – skaičius patikrinimui, r1, r2 – skaičiai nusakantys rėžius) ir grąžina boolean kintamąjį.
// Metodas turi:
//Patikrinti ar r1 < r2 – jeigu ne atspausdinti pranešimą ir baigti darbą panaudojant return.
//Patikrinti ir atpausdinti pranešimus ar sk yra tarp r1 r2 ar ne.
//Atitinkamai pagal rezultatą ar skaičius tarp r1 ir r2 grąžinti false arba true.
        trysParametrai(5,1,6);



    }

    private static boolean trysParametrai(int sk,int r1,int r2) {
        if (!(r1<r2)) {
                System.out.println("r1<r2 = False");
            return false;
        }
        if (sk>r1&&sk<r2) {
            System.out.println("sk>r1&&sk<r2 = taip");
            return true;
        }else{
            System.out.println("sk>r1&&sk<r2 = Ne");
            return false;
        }
    }


    private static boolean arTrizenklis(int sveikas){

        if (String.valueOf(sveikas).length()==3){
            return true;
        }else{
            return false;
        }


    }

    private static boolean checkIfNumberIsNegative(int sveikas){
        boolean aaa=false;
        if (sveikas<0)  aaa=true;
        return aaa;
    }


    private static int priimtiGrazintiSveika(int sveikas){
        return sveikas;
    }

    private static int priimtiGrazintiSveikaAtspausdinti(int sveikas){
        System.out.println("priimtiGrazintiSveikaAtspausdinti="+sveikas);
        return sveikas;
    }


    private static int priimtiDuSveikusGrazintiSuma(int sveikas1, int sveikas2){
        int sveikas=sveikas1+sveikas2;
        return sveikas;
    }

    private static String priimti2CharusGrazintiStringa(char a1,char a2){
        String atsakymas="";//=vienas+du;
        //System.out.println("char="+a1+a2);
        atsakymas=atsakymas+a1+a2;
        //ats[0]=vienas;
        //ats[1]=du;
        return atsakymas;
    }

    private static void priespaskutine(String sveikas){
        int intas=123;
        String atsakymas=sveikas+intas;
        System.out.println("Stringas plius sveikas="+atsakymas);
    }
    private static boolean loginis(){
        boolean atsakymas=true;
        return atsakymas;
    }

}
