package pagrindinis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Skaitymas  {
    public static void main(String[] args) {
        String big;
        int a=11;
        if (a>5&&a<10){
            big="tiese";
        }else {
            big="netiese";
        }
        big=a>5&&a<10?"tiese":"netiese";
        //System.out.println(big);
        //System.out.println(a>5&&a<10?"tiese":"netiese");

        big=a>5? a<10?"tinka":"pusiau tinka" :"netinka";
        System.out.println("bigas="+big);

        String aa="labas";
        boolean found=false;

        found=aa.equals("labas")?true:false;
        System.out.println("found="+found);



//        1 sukurti programą, kuri atspausdintų ar įvestas skaičius yra neigiamas
        System.out.println(arneigiamas());
//        2 sukurti programą, kuri pagal įvestą žmogaus amžių atspausdina ar žmogus gali balsuoti (turi virš 18 metų).
//        Apsaugoti programą nuo labai didelių (didesnių negu 100) ir neigiamų skaičių. Jeigu įvestas skaičius neteisingas – atspausdinti pranešimą.
        System.out.println(arGaliBalsuoti());

//        3 sukurti programą, kuri spausdintų atskirus pranešimus jeigu ivestas skaičius:
//        Neigiamas – „neigiamas“
//        Didesnis negu 100 „didesnis negu 100“
//        Nuo 40 iki 60 „tarp 40 ir 60“
//        Visais kitais atvejais parašytų kitą pranešimą.
        System.out.println(lyginamSkaicius());




/*        System.out.println("Iš kiek skaičių skaičiuosite vidurkį?");
        int kiekisSkaiciu=readNumber2(), suma=0, sandauga=1;
        for (int i = 1; i <= kiekisSkaiciu; i++) {
            System.out.println("Įveskite skaičių Nr."+i);
            suma+=readNumber2();
        }
        double atsakymas=0.0+suma;
        System.out.println(atsakymas/kiekisSkaiciu+0.0);*/

    }

    private static String lyginamSkaicius(){
        String atsakymas="kitas pranesimas";
        System.out.println("Iveskite skaiciu lyginimui:");

        int skaicius= Integer.parseInt(readNumber2());
        if (skaicius<0){
            atsakymas="Neigiamas";
        }
        if (skaicius>40&&skaicius<60){
            atsakymas="tarp 40 ir 60";
        }

        return  atsakymas;
    }

    private static String arGaliBalsuoti(){
        String atsakymas;
        Integer amzius = 0;
        String ivestis;
//  Paprastas budas verciant Stringa i Integeri
/*        System.out.println("Iveskite zmogaus amziu:");
        String ivestis = readNumber2();
        boolean success = false;
        while (!success) {
            try {
                amzius = Integer.parseInt(ivestis);
                success = true;
            } catch (Exception e) {
                System.out.println("Klaida. Iveskite sveika skaiciu");
                ivestis = readNumber2();
            }
        }*/
//Priskiriant ivedimo metode Integerio kintamajam rezultata is komandines eilutes
/*        System.out.println("Iveskite zmogaus amziu:");
        boolean success = false;
        while (!success) {
            try {
                amzius = readNumber4();
                success = true;
            } catch (Exception e) {
                System.out.println("Klaida. Iveskite sveika skaiciu");
            }
        }*/

//Isitikiname ar sveikas skaicius su specialiu metodu
/*        System.out.println("Iveskite zmogaus amziu:");
        boolean success1 = false;
        while (!success1) {
                ivestis = readNumber2();
                success1 = arInteger(ivestis);
                if (success1) amzius=Integer.parseInt(ivestis);
                else System.out.println("Klaida. Iveskite sveika skaiciu");
        }*/

////////

//be try catch

        sveikasSkaiciusIsScanerio();

////////////////////////////////////////////////
        if (amzius<0||amzius>100){
            atsakymas="Klaida";
            return atsakymas;
        }
        if (amzius>=18){
            atsakymas="Gali balsuoti";
        }else {
            atsakymas="Negali balsuoti";
        }
        return  atsakymas;
    }

    private static String arneigiamas(){
        String atsakymas;
        System.out.println("Iveskite skaiciu:");
        if (Integer.parseInt(readNumber2())<0){
            atsakymas="Neigiamas";
        }else {
            atsakymas="Teigiamas arba nulis";
        }
        return  atsakymas;
    }

    public static void ivestiIrSuskaiciuotiVidurki () {
        int a1=0,a2=0,a3=0;
        String ats;
        System.out.println("Iveskite pazymius:");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try{
            ats = br.readLine();
            a1 = Integer.parseInt(ats);
            ats = br.readLine();
            a2 = Integer.parseInt(ats) ;
            ats = br.readLine();
            a3 = Integer.parseInt(ats);
        }
        catch(Exception e){
            System.out.println("Write an integer number"); //This is what user will see if he/she write other thing that is not an integer
        }
        System.out.println("Vidurkis:"+(a1+a2+a3)/3);

    }

    private static String readNumber2(){ //throws IOException{
        Scanner scaner = new Scanner(System.in);
        String readNumber = scaner.nextLine();
        return readNumber;
    }

    private static int readNumber4()throws Exception { //throws IOException{
        Scanner scaner = new Scanner(System.in);
        int readNumber = scaner.nextInt();
        return readNumber;
    }

    public static boolean arInteger(String s) {
        try{
            Integer.parseInt(s);
            return true;
        }catch (NumberFormatException ex){
            return false;
        }
    }
    public static int sveikasSkaiciusIsScanerio() {
        System.out.println("Iveskite zmogaus amziu:");
        Scanner sc = new Scanner(System.in);
        while (!sc.hasNextInt()) {
            System.out.println("Klaida. Iveskite sveika skaiciu");
            sc.next();
        }
        return  sc.nextInt();
    }

/*    private static int readNumber2()throws IOException{ //throws IOException{
        Scanner scaner = new Scanner(System.in);
        int readNumber = scaner.nextInt();
        return readNumber;
    }*/

/*    private static int readNumber3(){ //throws IOException{
        Scanner scaner = new Scanner(System.in);
        int readNumber = scaner.nextInt();
        return readNumber;
    }*/

}