package pagrindinis;

import java.util.Scanner;

public class Switch {
    public static void main(String[] args) {
//        Susikurti paprastą switch sąkinį su keliom salygom su skaičiais. Pabandyti kas vyksta idėjus/pašalinus break sakinį.
        int skaicius=2;
        switch (skaicius) {
            case 0:
                System.out.println("skaicius="+skaicius );
                break;
            case 1:
                System.out.println("skaicius="+skaicius );
                break;
            case 2:
                System.out.println("skaicius="+skaicius );
                break;
            default:
                System.out.println("neradom" );
                break;
        }
//        Sukurti paprastą switch sakinį, kuris turėtų kelias sąlygas – jeigu įvestas 1, 2, 3 arba 5 – tada atspausdintų „Įvestas žinomas skaičius:  1“ (arba 1, 2,3,5).
//        Jeigu įvestas bet koks kitas skaičius – atspausdintų „Įvestas nežinomas skaičius“. Po to – sukurti metodą, kuris mums padėtų patogiau atspausdinti „Įvestas žinomas skaičius:  x“
        switch (skaicius) {
            case 1:case 2:case 3: case 5:
                spausdintiPatogiai("Įvestas žinomas skaičius:  1");
                spausdintiPatogiai2(skaicius);
                //System.out.println("Įvestas žinomas skaičius:  1" );
                break;
            default:
                //System.out.println("Įvestas nežinomas skaičius" );
                spausdintiPatogiai("Įvestas nežinomas skaičius");
                spausdintiPatogiai2(skaicius);
                break;
        }


//        Sukurti programą su meniu, kuri  atspausdintų meniu punktus 1. 2. 3. ir tt. su paaiškinimais.
//        Tada paprašytų įvesti meniu pasirinkimą kaip skaičių ir panaudojant switch sakinį atitinkamai atliktų pagal skaičių pasirinktą veiksmą:
//                Paprašytų įvesti du skaičius ir juos sudėtų ir atspausdintų rezultatą.
//                Paprašytų įvesti du skaičius ir sudaugintų du skaičius; Sukurti dviejų skaičių dauginimo metodą.
//                Atspausdintų 256 skaičių pakeltą kvadratu.
//                Jeigu nepasirinktas 1-3 punktas, atspausdintų atitinkamą pranešimą.
//                Kai sužinosim kas yra ciklai - padaryti kad programa vyktų be galo, iki tol kol įvesime 0.
        int skaicius2=-1;
        while (!(skaicius2==0)) {
            System.out.println("1.");
            System.out.println("2.");
            System.out.println("3.");
            System.out.println("Nurodykite meniu punkta:");
            skaicius2 = Integer.parseInt(readNumber2());


            switch (skaicius2) {
                case 1:
                    System.out.println("Iveskite du skaicius:");
                    int aa1 = readNumber();
                    ;
                    int aa2 = readNumber();
                    ;
                    ;
                    System.out.println("Sudetis:" + (aa1 + aa2));
                    break;
                case 2:
                    System.out.println("Iveskite du skaicius:");
                    int aaa1 = readNumber();
                    ;
                    ;
                    int aaa2 = readNumber();
                    ;
                    ;
                    skaiciutiSandauga(aaa1, aaa2);
                    break;
                case 3:
                    System.out.println("skaicius256kvadratu=" + (256 * 256));
                    break;
                default:
                    System.out.println("nepasirinktas 1-3 punktas");
                    break;
            }
        }

    }

    private static void spausdintiPatogiai(String komentaras){
        System.out.println(komentaras );
    }
    private static void spausdintiPatogiai2(Integer skaic){
        System.out.println("Įvestas žinomas skaičius:  "+skaic );
    }

    private static int readNumber(){ //throws IOException{
        Scanner scaner = new Scanner(System.in);
        String readNumber = scaner.nextLine();
        return Integer.valueOf(readNumber) ;
    }

    private static String readNumber2(){ //throws IOException{
        Scanner scaner = new Scanner(System.in);
        String readNumber = scaner.nextLine();
        return readNumber;
    }

    private static void skaiciutiSandauga(Integer skaic1,Integer skaic2){
        System.out.println("Sandauga:  "+(skaic1*skaic2) );
    }

}
