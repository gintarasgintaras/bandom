package puodelisPackage;

import kava.Produktai;

//        Sukurti klasę kavos puodelis kuri praplėčia Puodelis:​
public class KavosPuodelis extends Puodelis {

//        Turi atributą Produktai, kuris saugo kiek kokių produktų įdėta į kavos puodelį​
          //Produktai produktai=new Produktai();
          Produktai produktai=new Produktai();
//        Turėtų kavos pavadinimą​
          public String kavosPavadinimas;
//        Turėtų boolean atributą, kuris pasakytų ar kava pagamintą​
          public boolean kavaPagaminta;
//        Turėtų metodą toString, kuris atspausdintų informaciją​

          public KavosPuodelis(String kavosPavadinimas,int vanduo, int cukrus, int kava){
              this.kavosPavadinimas=kavosPavadinimas;
              this.produktai.cukrausKiekisG=cukrus;
              this.produktai.vandensKiekisL=vanduo;
              this.produktai.kavosPupeliuKiekisG=kava;
          }

          public Produktai gautiKavosPuodelioProduktai  (){
              return this.produktai;
          }

          @Override
          public String toString (){
              System.out.println("Informacija: ");
              System.out.println("Kavos pavadinimas: "+kavosPavadinimas);
              return "sfgdg";
          }
//        Sukurti savo klases pagal kavos tipą, kurios praplečia klasę KavosPuodelis (Pvz.: CapuccinoPuodelis):​

//        Turi konstruktorių, kuris inicializuoją kavos vardą ir produktų kiekius​

//        Kavos aparato gaminkKava metodas turėtų:​
//        Sukurti atitinkamo tipo kavos puodelį​
//        Sukurti metodą gamink kavą, kuris priima kavos puodelį, kuris pagal kavos puodelio produkto kiekius sumažintų kavos aparato produktų kiekius ir pakeistų ‚ar kava pagaminta‘ į true​
//        Grąžintų atitinkamo tipo kavos puodelį su pagaminta kava
}
