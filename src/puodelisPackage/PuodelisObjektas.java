package puodelisPackage;

import gyvunas.Katinas;
import kava.KavosAparatas;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PuodelisObjektas {
    public static void main(String[] args) {
        ///KavosPuodelis kavosPuodelis = new KavosPuodelis("");
        ///kavosPuodelis.kavosPavadinimas="Kapucino";
        ///kavosPuodelis.toString();
        int kavosPuodoId=4; //1-3

        KavosAparatas kavosAparatas=new KavosAparatas();
        //public void plovimas(){

        kavosAparatas.papildyti(9,9,9);
        switch (kavosPuodoId){
            case 1:
                JuodosKavosPuodelis juodosKavosPuodelis=new JuodosKavosPuodelis();
                kavosAparatas.gaminkKava(juodosKavosPuodelis);
                break;
            case 2:
                BaltosKavosPuodelis baltosKavosPuodelis=new BaltosKavosPuodelis();
                kavosAparatas.gaminkKava(baltosKavosPuodelis);
                break;
            case 3:
                CapuccinoPuodelis capuccinoPuodelis=new CapuccinoPuodelis();
                kavosAparatas.gaminkKava(capuccinoPuodelis);
                break;
            default:
                BeleKokiosKavosPuodelis beleKokiosKavosPuodelis=new BeleKokiosKavosPuodelis();
                kavosAparatas.gaminkKava(beleKokiosKavosPuodelis);
                break;
        }

        Set<Integer> set=new HashSet<>();
        set.add(10);
        set.add(11);
        set.add(5);
        set.add(11);
        System.out.println(set);

/*        Iterable iter=set.iterator();
        while (iter.hashCode()){
            System.out.println(iter.n);
        }*/
        List<Integer,Integer> listas = new ArrayList<Integer,Integer>();
//        Sukurti klasę Puodelis:​
//        Turi int atributą kuris nusako maksimalią puodelio talpą​
//        Turi metodą rodykInformaciją, kuris atspausdina kokios talpos puodelis​
//        Sukurti klasę kavos puodelis kuri praplėčia Puodelis:​
//        Turi atributą Produktai, kuris saugo kiek kokių produktų įdėta į kavos puodelį​
//        Turėtų kavos pavadinimą​
//        Turėtų boolean atributą, kuris pasakytų ar kava pagamintą​
//        Turėtų metodą toString, kuris atspausdintų informaciją​
//        Sukurti savo klases pagal kavos tipą, kurios praplečia klasę KavosPuodelis (Pvz.: CapuccinoPuodelis):​
//        Turi konstruktorių, kuris inicializuoją kavos vardą ir produktų kiekius​
//        Kavos aparato gaminkKava metodas turėtų:​
//        Sukurti atitinkamo tipo kavos puodelį​
//        Sukurti metodą gamink kavą, kuris priima kavos puodelį, kuris pagal kavos puodelio produkto kiekius sumažintų kavos aparato produktų kiekius ir pakeistų ‚ar kava pagaminta‘ į true​
//        Grąžintų atitinkamo tipo kavos puodelį su pagaminta kava
    }
}
